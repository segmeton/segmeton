<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Faker\Factory as Faker;

class GlobalController extends Controller
{
    public function index()
    {
        $faker = Faker::create();

        $dummy = $faker->paragraphs($nb = 20, $asText = false);

        return view('welcome', compact('dummy'));
    }
}
