<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Scripts -->
        <script src="{{ asset('js/maintenance.js') }}" defer></script>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/maintenance.css') }}" rel="stylesheet">
    </head>
	<body>
		<div class="container">
			<div class="content">
                <div class="earth planet"></div>
                <div class="title">Be right back.</div>
                <div class="message">{{ json_decode(file_get_contents(storage_path('framework/down')), true)['message'] }}</div>
            </div>
            <footer>
                &copy; @php echo date("Y"); @endphp Copyright. {{ config('app.name', 'Laravel') }}.
            </footer>
        </div>
	</body>
</html>
