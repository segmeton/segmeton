<div class="row">
    <div class="col text-center">

        <div class="notification text-center">

            @if ( isset($errors) AND count($errors) > 0 )
                <div class="flash-message">
                    <div class="alert alert-danger alert-many" role="alert">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif

            @if ( session('error') )
                <div class="flash-message">
                    <div class="alert alert-danger" role="alert">
                        {{session('error')}}
                    </div>
                </div>
            @endif

            @if ( session('warning') )
                <div class="flash-message">
                    <div class="alert alert-warning" role="alert">
                        {{session('warning')}}
                    </div>
                </div>
            @endif

            @if ( session('success') )
                <div class="flash-message">
                    <div class="alert alert-success" role="alert">
                        {{session('success')}}
                    </div>
                </div>
            @endif

            @if ( session('info') )
                <div class="flash-message">
                    <div class="alert alert-info" role="alert">
                        {{session('info')}}
                    </div>
                </div>
            @endif

            @if ( session('primary') )
                <div class="flash-message">
                    <div class="alert alert-primary" role="alert">
                        {{session('primary')}}
                    </div>
                </div>
            @endif

            @if ( session('secondary') )
                <div class="flash-message">
                    <div class="alert alert-secondary" role="alert">
                        {{session('secondary')}}
                    </div>
                </div>
            @endif

            @if ( session('light') )
                <div class="flash-message">
                    <div class="alert alert-light" role="alert">
                        {{session('light')}}
                    </div>
                </div>
            @endif

            @if ( session('dark') )
                <div class="flash-message">
                    <div class="alert alert-dark" role="alert">
                        {{session('dark')}}
                    </div>
                </div>
            @endif

        </div>

    </div>
</div>
