<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-6">
                    <div class="owner text-left">Owner-san</div>
            </div>
            <div class="col-12 col-md-6">
                <div class="copyright text-right">
                    &copy; @php echo date("Y"); @endphp Copyright. {{ config('app.name', 'Laravel') }}.
                </div>

            </div>
        </div>
    </div>
</footer>
